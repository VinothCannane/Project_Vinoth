package week6.day1;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CW_CL extends projectmethods {

	@Test/* (groups= "smoke")*/(dataProvider = "createLead")
	public void create(String cmpyName, String fName, String lName, String ph, String email, String verify){

		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement elecrtlead = locateElement("PartialLinkText","Create Lead");
		click(elecrtlead);
		WebElement cmpyname = locateElement("id","createLeadForm_companyName");
		type(cmpyname, cmpyName);
		WebElement frstname = locateElement("id","createLeadForm_firstName");
		type(frstname, fName);
		WebElement lastname = locateElement("id","createLeadForm_lastName");
		type(lastname, lName);
		WebElement src = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(src, "Employee");
		WebElement MKC = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(MKC, "Car and Driver");
		WebElement phneno = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phneno, ph);
		WebElement emailid = locateElement("id","createLeadForm_primaryEmail");
		type(emailid, email);
		WebElement submt = locateElement("name","submitButton");
		clickWithNoSnap(submt);
		WebElement fname = locateElement("id", "viewLead_firstName_sp");
		String text=getText(fname);
		verifyExactText(text, verify);
	}
	@DataProvider(name = "createLead")
	public Object[][] getdata()
	{
		Object[][] data = new Object [2][6];
		data[0][0] = "Test Leaf";
		data[0][1] = "Sujith";
		data[0][2]="Thupili";
		data[0][3]="89864652";
		data[0][4]="sujith@hexaware.com";
		data[0][5]="Sujith";
		data[1][0] = "CTS";
		data[1][1] = "Vinoth";
		data[1][2]="Selvam";
		data[1][3]="9944929907";
		data[1][4]="vinoth@hexaware.com";
		data[1][5]="Vinoth";

		return data;

	}

}

