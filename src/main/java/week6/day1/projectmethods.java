package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class projectmethods extends SeMethods{
	@BeforeMethod
	@Parameters({"browser","appurl","username","password"})
	public void login(String browse, String url, String uName, String uPass) {
		startApp(browse, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, uPass);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}

	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}

}
