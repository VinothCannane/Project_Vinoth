package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class CW_Edit_Lead extends projectmethods{
	@ Test /*(dependsOnMethods = "week6.day1.CW_CL.create", groups ="sanity", dependsOnGroups = "smoke")*/(dataProvider = "fetchdata")
	public void edit(String fname, String Cmpname)
	{

		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement leadele = locateElement("LinkText", "Leads");
		click(leadele);
		WebElement findleads = locateElement("LinkText", "Find Leads");
		click(findleads);
		WebElement firstname = locateElement("xpath", "(//div[@class='x-form-clear-left'])[19]/..//input");
		type(firstname, fname);
		WebElement fndleadbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndleadbutton);
		WebElement fistlink = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
		click(fistlink);
		verifyTitle("View Lead | opentaps CRM");
		WebElement Edit = locateElement("xpath", "//a[contains(text(),'Edit')]");
		click(Edit);
		WebElement editcompanyname = locateElement("id", "updateLeadForm_companyName");
		editcompanyname.clear();
		type(editcompanyname, Cmpname);
		WebElement update = locateElement("name", "submitButton");
		click(update);
		WebElement Updatedcompany = locateElement("id", "viewLead_companyName_sp");
		getText(Updatedcompany);
		verifyExactText("Hexaware", "Hexaware");
		closeBrowser();
	}
}