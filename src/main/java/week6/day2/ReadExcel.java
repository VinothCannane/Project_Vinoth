package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData (String fileName)throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");// locate workbook and read a file
		XSSFSheet sheet = wbook.getSheet(fileName); // get sheet and always read data from the sheet
		int lastRowNum = sheet.getLastRowNum(); // getting the last row
		int lastCellNum = sheet.getRow(0).getLastCellNum(); // getting the last cell number 
		Object[][] data=  new Object[lastRowNum][lastCellNum]; // putting the 2d data in the object array
		for (int i=1; i<=lastRowNum; i ++)
		{
			XSSFRow row = sheet.getRow(i); // calling all rows  one by one
			for (int j= 0; j <lastCellNum; j++)
			{
				XSSFCell cell= row.getCell(j); // getting all columns one bye one
				String Stringcellvalue = cell.getStringCellValue();
				data[i-1][j]= Stringcellvalue;  // since the excel sheet start with 2nd row and column 1 we are using i-1
				System.out.println(Stringcellvalue);

			}
		}
		wbook.close();
	return data;   // returning data to another class
	}
}

