package week2.day2;

import java.util.Scanner;

public class Exceptionswork {
	
	public void call() throws ArrayIndexOutOfBoundsException
	{
		try {
			String ch[] = {"Vinoth","Rajesh","Ramesh","Vignesh","Suresh" };
			Scanner scan = new Scanner (System.in);
			System.out.println(ch[6]);
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				System.out.println("The value entered is out of Bound in the array");
			}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			System.out.println("It will always run");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Exceptionswork eum= new Exceptionswork();
		eum.call();
//		throw new ArrayIndexOutOfBoundsException();
//		try {
//		String ch[] = {"Vinoth","Rajesh","Ramesh","Vignesh","Suresh" };
//		System.out.println(ch[6]);
//		}
//		catch(ArrayIndexOutOfBoundsException e)
//		{
//			System.out.println("The value entered is out of Bound in the array");
//		}
	}

}
