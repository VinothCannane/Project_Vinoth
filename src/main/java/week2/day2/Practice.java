package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.iterators.EntrySetMapIterator;

public class Practice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//List - allows duplicate and always in order in which it has been added
		List<String> strobj = new ArrayList<String>();
		
		strobj.add("Vinoth is  very good boy");
		strobj.add("I am from pondicherry");
		strobj.add("I studied in Petit Seminaire Higher secondary school");
		strobj.add("I studied in Petit Seminaire Higher secondary school");

		int size = strobj.size();
		System.out.println("Number of String items in the List : "+ size);
//		System.out.println(strobj.get(2));
//		Collections.sort(strobj);
//		for (String eachstring : strobj) {
//			System.out.println(eachstring);
//		}
		strobj.remove(1);
		System.out.println(strobj);
	strobj.clear();
	System.out.println(strobj);

	
	// Linked hash set - No duplicates and order in which it has been added
	// Hash set - no duplicates - random order
	//Tree set - no duplicates - ASCII order
	Set<String> setobj= new TreeSet<String>();
	setobj.add("Vinoth");
	setobj.add("Karthik");
	setobj.add("Karthik");
	setobj.add("Selvam");
	setobj.add("Ally");
	
	System.out.println(setobj);
	
	// Hashmap -  if 2 Dimensional values used
	Map<String, Integer> mapobj= new HashMap<String, Integer>();
	mapobj.put("Vinoth", 45);
	mapobj.put("chandra", 65);
	mapobj.put("Rajesh", 89);
	
	System.out.println(mapobj);
	mapobj.put("Rajesh", 92);
	mapobj.remove("Vinoth");
	for (Entry<String, Integer> eachname : mapobj.entrySet()) {
		System.out.println(eachname.getKey());
		System.out.println(eachname.getValue());
	}
	Integer num = mapobj.get("chandra");
	System.out.println(num);
	String txt= "VINOT4564S";
	System.out.println(txt.replaceAll("\\d",""));
	
	String pattern = "[A-Z]{5}\\d{4}[A-Z]";
	Pattern p = Pattern.compile(pattern);
	Matcher matcher = p.matcher(txt);
	System.out.println(matcher.matches());
}}
