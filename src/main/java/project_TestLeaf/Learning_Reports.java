package project_TestLeaf;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;



public class Learning_Reports {
	
	public static ExtentReports extent;
	public String TCName, Description, Author, Category, excelFileName;
	public static ExtentTest test;
	@BeforeSuite
	public void initialize(){
		ExtentHtmlReporter report = new ExtentHtmlReporter("./Report/report.html");
		report.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(report);
	}
	
	@BeforeMethod
	public void Data() {
		test = extent.createTest(TCName, Description);
		test.assignAuthor(Author);
		test.assignCategory(Category);
	}
	
	public void Result(String output, String Status) {
		if(Status.equalsIgnoreCase("Pass")) {
			test.pass(output);
		}if(Status.equalsIgnoreCase("fail")) {
			test.fail(Status);
		}
	}
	
	@AfterSuite
	public void LastExecution() {
		extent.flush();
	}
}