package project_TestLeaf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCarWebPage extends SeMethods{
	
	@BeforeClass
	public void Data1() {
		TCName = "ZoomCar TestCase";
		Description = "This Testcase will book a car";
		Author = "Pragash";
		Category = "Usability Testing";
	}
	
	@Test
	public void zoomcar(){
		startApp("Chrome", "https://www.zoomcar.com");
		WebElement chennai = locateElement("xpath", "//img[@alt='Chennai']");
		click(chennai);
		WebElement startJourney = locateElement("xpath", "//a[text()='Start your wonderful journey']");
		click(startJourney);
		WebElement location = locateElement("xpath","//div[contains(text(),'Thuraipakkam')]");
		click(location);
		WebElement nextButton = locateElement("xpath", "//button[text()='Next']");
		click(nextButton);
		WebElement date = locateElement("xpath", "//div[text()='Mon']");
		click(date);
		click(nextButton);
		WebElement verifyDate = locateElement("xpath", "//div[text()='Mon']");
		//String Date = verifyDate.getText();
		String Date = getText(verifyDate);
		System.out.println(Date);
		if(Date.contains("MON")) {
			System.out.println("The Required date is selected");
		}else {
			System.out.println("Required date is not selected");
		}
		WebElement doneButton = locateElement("xpath", "//button[text()='Done']");
		click(doneButton);
		List<WebElement> allCars = driver.findElementsByXPath("//div[@class='car-list-layout']/div[*]");
		System.out.println(allCars.size());
		List<WebElement> priceOfCars = driver.findElementsByXPath("//div[@class='car-amount']//div[@class='price']");
		List<String> value= new ArrayList<>();
		String replaceAll;
		for (WebElement Price : priceOfCars) {
			
			String PriceAll = Price.getText();
			replaceAll = PriceAll.replaceAll("^[0-9]", "");
			System.out.println(replaceAll);
			value.add(replaceAll);
		}
		Collections.sort(value);
		for (String allSortedValue : value) {
			System.out.println("The Price is "+allSortedValue);
		}
		int size = value.size();
		String highRate = value.get(size-1);
		System.out.println("High rate is: "+highRate);
		String carName = driver.findElementByXPath("//div[contains(text(),"+highRate+")]/preceding::h3").getText();
		System.out.println("Name of the car which has highest rate is: "+carName);
		
	}
	
}
