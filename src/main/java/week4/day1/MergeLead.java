package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img").click();
		String windowHandle = driver.getWindowHandle();
		Set<String> allwindows = driver.getWindowHandles();
		List<String> listwindows = new ArrayList<String>();
		listwindows.addAll(allwindows);
		driver.switchTo().window(listwindows.get(1));
		driver.findElementByXPath("(//div[@class='x-form-element']/input)[1]").sendKeys("10039");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(listwindows.get(0));
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img").click();
		Set<String> allwindows1 = driver.getWindowHandles();
		List<String> listwindows1 = new ArrayList<String>();
		listwindows1.addAll(allwindows1);
		driver.switchTo().window(listwindows1.get(1));
		driver.findElementByXPath("(//div[@class='x-form-element']/input)[1]").sendKeys("10053");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(listwindows.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//div[@class='x-form-item x-tab-item']//input").sendKeys("10021");
		driver.findElementByXPath("//button[text () ='Find Leads']").click();
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./Snaps/img.png");
		FileUtils.copyFile(src, dest);
		driver.quit();
	}
}

