package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		String WindowID = driver.getWindowHandle();
		System.out.println("The Window Id is : "+ WindowID); /// to get unique window id
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println("Number of windows opened: " +allwindows.size()); // get the count of windows opened
		List<String> lstwindows= new ArrayList<String>(); // pushing set to list to toggle the windows
		lstwindows.addAll(allwindows);    // list of all windows
		String secondwindow1 = lstwindows.get(1); 
		driver.switchTo().window(secondwindow1);
		System.out.println(driver.getTitle());   //getting title of second window
		System.out.println(driver.getCurrentUrl());  // getting URL of second window
		driver.quit();   // closing the windows
		
	}

}
