package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Drag_Drop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://jqueryui.com/draggable/");
		driver.manage().window().maximize();
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']/p");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, 100, 100).perform();
		driver.findElementByLinkText("Droppable").click();
		driver.switchTo().frame(0);
		Actions builder1 = new Actions(driver);
		WebElement drag2 = driver.findElementByXPath("//div[@id='draggable']/p");
		WebElement drop2 = driver.findElementByXPath("//div[@id='droppable']/p");
		builder1.dragAndDrop(drag2, drop2).perform();


	}

}
