package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;


public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByPartialLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Company1");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vinoth");
		driver.findElementById("createLeadForm_lastName").sendKeys("Selvam");
		driver.findElementByName("submitButton").click();
	}

}
