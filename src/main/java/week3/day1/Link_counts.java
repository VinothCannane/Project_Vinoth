package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Link_counts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		List<WebElement> alllinks = driver.findElementsByTagName("a");
		System.out.println("Number of hyperlinks available in the page: "+alllinks.size());
		for (WebElement eachlink : alllinks) {
			System.out.println(eachlink.getText());
		}
	    alllinks.get(1).click();
		

	}

}
