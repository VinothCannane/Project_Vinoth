package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Dropdowns {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe"); // Mandatory property
		ChromeDriver driver = new ChromeDriver(); // invoking chrome browser
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");// Enter user name and password
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click(); // clicking the hyperlink
		driver.findElementByPartialLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Company1");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vinoth");
		driver.findElementById("createLeadForm_lastName").sendKeys("Selvam");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");// creating object for the dropdown
		Select dd= new Select (src); // creating obj for select class
		dd.selectByVisibleText("Conference");
		WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd2= new Select (src1);
		dd2.selectByValue("CATRQ_CARNDRIVER");
		List<WebElement> option2 = dd2.getOptions(); // Storing the dropdown values in the list
		for (WebElement option23 : option2) {
			System.out.println(option23.getText());
		}
		driver.findElementByName("submitButton").click();	// printing the dropdown values
	}
}

