package week3.day1;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Snapshot {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		// ScreenShot
		// store output as file
//		File src = driver.getScreenshotAs(OutputType.FILE);
//		//File source = driver.getScreenshotAs(OutputType.FILE);
//		// store the file in the specific path
//		File dest = new File("./Snaps/img.png");
//		// Copy src to dest
//		FileUtils.copyFile(src, dest);
		
	File src = driver.getScreenshotAs(OutputType.FILE);
	File dest = new File("./Snaps/img.png");
	FileUtils.copyFile(src, dest);}

}
