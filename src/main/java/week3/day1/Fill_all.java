package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Fill_all {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByPartialLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Hexaware Technologies");
		driver.findElementById("createLeadForm_parentPartyId").sendKeys("10423");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vinoth");
		driver.findElementById("createLeadForm_lastName").sendKeys("Selvam");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select droplist1= new Select(src);
		droplist1.selectByVisibleText("Existing Customer");
		WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select droplist2= new Select(src2);
		droplist2.selectByValue("DEMO_MKTG_CAMP");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Vino");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Sendamare cannane");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Salute");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("General Profile");
		driver.findElementById("createLeadForm_departmentName").sendKeys("EEE");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("213546");
		WebElement src3 = driver.findElementById("createLeadForm_currencyUomId");
		Select droplist3 =new Select (src3);
		droplist3.selectByVisibleText("INR - Indian Rupee");
		WebElement src4 = driver.findElementById("createLeadForm_industryEnumId");
		Select droplist4 =new Select (src4);
		droplist4.selectByVisibleText("Computer Software");
		WebElement src5 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select droplist5 =new Select (src5);
		droplist5.selectByVisibleText("S-Corporation");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("450");
		driver.findElementById("createLeadForm_sicCode").sendKeys("IRC");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("symbol");
		driver.findElementById("createLeadForm_description").sendKeys("Entering description");
		driver.findElementById("createLeadForm_importantNote").sendKeys("None");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("605");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9944929907");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Rajesh");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("vinoth.fuso@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Kumar");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Kuttala");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("70, Needarajapier street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Near Amudhasurabi");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Pondicherry");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("605001");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("0413");
		WebElement src6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select droplist6 =new Select (src6);
		droplist6.selectByVisibleText("India");
		Thread.sleep(3000);
		WebElement src7 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select droplist7 =new Select (src7);
		droplist7.selectByVisibleText("TAMILNADU");
		driver.findElementByName("submitButton").click();
	}

}
