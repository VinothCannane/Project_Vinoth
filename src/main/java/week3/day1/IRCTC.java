package week3.day1;

import javax.swing.JComboBox.KeySelectionManager;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTC {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		driver.findElementByName("userRegistrationForm:userName").sendKeys("vinothfuso");
		driver.findElementById("userRegistrationForm:password").sendKeys("hemapriya");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("hemapriya");
		WebElement src1 = driver.findElementById("userRegistrationForm:securityQ");
		Select obj1 = new Select(src1);
		obj1.selectByVisibleText("What make was your first car or bike?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Bajaj");
		WebElement src2 = driver.findElementById("userRegistrationForm:prelan");
		Select obj2 = new Select(src2);
		obj2.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Vinoth");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Selvam");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Sendamare Cannane");
		driver.findElementByName("userRegistrationForm:gender").click();
		driver.findElementByName("userRegistrationForm:maritalStatus").click();
		WebElement src3 = driver.findElementById("userRegistrationForm:dobDay");
		Select obj3 = new Select(src3);
		obj3.selectByVisibleText("20");
		WebElement src4 = driver.findElementById("userRegistrationForm:dobMonth");
		Select obj4 = new Select(src4);
		obj4.selectByVisibleText("DEC");
		WebElement src5 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select obj5 = new Select(src5);
		obj5.selectByVisibleText("1991");
		WebElement src6 = driver.findElementById("userRegistrationForm:occupation");
		Select obj6 = new Select(src6);
		obj6.selectByVisibleText("Professional");
		WebElement src7 = driver.findElementById("userRegistrationForm:countries");
		Select obj7 = new Select(src7);
		obj7.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("vinoth.fuso@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9944929907");
		WebElement src8 = driver.findElementById("userRegistrationForm:nationalityId");
		Select obj8 = new Select(src8);
		obj8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("Door No: 70");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("605001",Keys.TAB);
		Thread.sleep(3000);
		WebElement src10 = driver.findElementById("userRegistrationForm:cityName");
		Select obj10 = new Select(src10);
		obj10.selectByVisibleText("Pondicherry");
		Thread.sleep(3000);
		WebElement src9 = driver.findElementByName("userRegistrastionForm:postofficeName");
		Select obj9 = new Select(src9);
		obj9.selectByVisibleText("Pondicherry H.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("9944929907");
	}

}
