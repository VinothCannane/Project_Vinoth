package week1.day2;

import java.util.Scanner;

public class String_methods {

	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner (System.in);
		System.out.println("Enter the name which you want to perform for string methods");
		String name = scan.next();

		char ch=name.charAt(5);
		System.out.println("The character value at 5 is :" +ch);
		String sub=name.substring(4,9);
		System.out.println("The Substring value is :" + sub);
		String join= name.concat("Test Leaf");
		System.out.println("The concatinated String is : " + join);
		String rep= name.replace("Vinoth", "Karthik");
		System.out.println("The replaced String is : " + rep);
		int value= name.indexOf('i');
		System.out.println("The Index value of i in the String entered is : "+ value);
	}

}
