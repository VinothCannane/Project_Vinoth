package week1.day2;

import java.util.Scanner;

public class Palindrome {

	static int number;
	public static void main(String[] args) {
		int number,a;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number : ");
		number=scan.nextInt();
		a=number;
		int rem=0,rev;
		for(rev=0;a>0;a=a/10)
		{
			rem =a%10;
			rev=rev*10 +rem;

		}
		System.out.println("Reversed number is " +rev);
		if(rev==number) {
			System.out.println("Entered number is a palindrome");
		}
		else {
			System.out.println("Entered Number is not a palindrome");
		}
	}
}
