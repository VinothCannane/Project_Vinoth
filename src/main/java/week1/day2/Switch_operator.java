package week1.day2;

import java.util.Scanner;

public class Switch_operator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		//for (int i=0 ; i<5; i++) {
			
		System.out.println("Please enter the Mobile code for the service provider");
		int input=scan.nextInt();

		switch (input)
		{
		case 944:
			System.out.println("Your Mobile service provider is BSNL");
			break;
		case 900:
			System.out.println("Your Mobile service provider is Airtel");
			break;
		case 897:
			System.out.println("Your Mobile service provider is Idea");
			break;
		case 630:
			System.out.println("Your Mobile service provider is Jio");
			break;
		default:
			System.out.println("The Mobile code you have entered is invalid");
		}


	}

}

//}