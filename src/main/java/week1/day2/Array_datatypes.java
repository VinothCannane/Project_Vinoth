package week1.day2;

public class Array_datatypes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long mobileNumber []= {9944929907l, 9944929908l,9944929905l,9944929903l};
		String phoneMake[] = {"Japan","China","India","US","Italy"};
		int prize[]= {50054,8989,65685,656,777};
		float camera_pixel[]= {13.2f,4.0f,22.0f,0.8f};
		boolean Isdual_sim;
		char fourGVolte[] = { 'Y','N'};
		short s[]= {45,58,65,72,47}; 
		double size[]= {5.526,8.457,6.23,7.24,2.45};
		System.out.println("The mobile Numbers are : ");
	for (long num:mobileNumber)
	{
		System.out.println(num);
	}
	System.out.println("The Phone Makes are : ");
	for (int i = 0; i < phoneMake.length; i++) {
		String mak = phoneMake[i];
		System.out.println(mak);
	}
	System.out.println("The Prize of the Mobiles are : ");
	for (int priz:prize)
	{
		System.out.println(priz);
	}
	System.out.println("The Mobile Camer Pixels are : ");
	for (float pix:camera_pixel)
	{
		System.out.println(pix);
	}
	System.out.println("Options for 4g Volte : ");
	for (char vol:fourGVolte)
	{
		System.out.println(vol);
	}
	System.out.println("Short : ");
	for (short sh:s)
	{
		System.out.println(sh);
	}
	System.out.println("The size of the phones are : ");
	for (double db:size)
	{
		System.out.println(db);
	}

}
}