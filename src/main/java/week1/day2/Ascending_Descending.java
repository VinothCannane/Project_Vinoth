package week1.day2;

import java.util.Scanner;

public class Ascending_Descending {

	public static void main(String[] args)
	{
		int temp;
		// TODO Auto-generated method stub
		Scanner scan = new Scanner (System.in);
		System.out.println("Enter the count of numbers to be sorted : ");
		int count = scan.nextInt();
		int num[]=new int [count];
		System.out.println("Enter the numbers to be sorted : ");
		for (int i=0; i<count; i++)
		{
			num[i] = scan.nextInt();
		}
		for (int i=0; i<count; i++)
		{
			for (int j= i+1; j<count; j++)
			{
				if (num[i]>num[j])
				{
					temp=num[i];
					num[i]=num[j];
					num[j]=temp;

				}
			}
		}
		System.out.println("The Ascending order is : ");
		for (int i=0; i < count ; i ++)
		{
			System.out.println(num[i]);	
		}


		for (int i=0; i <count ; i++)
		{
			for (int j=i+1; j <count; j++)
			{
				if (num[i]<num[j])
				{
					temp=num[i];
					num[i]=num[j];
					num[j]=temp;
				}
			}
		}
		System.out.println("The Descending order is : ");
		for (int i=0; i < count; i ++)
		{
			System.out.println(num[i]);	
		}
	}

}