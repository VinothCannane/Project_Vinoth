package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC002_Editlead extends SeMethods {
@ Test
	public void edit()
	{
		startApp("Chrome", "http://leaftaps.com/opentaps/");
		WebElement idele = locateElement("id", "username");
		type(idele, "DemoSalesManager");
		WebElement passwordele = locateElement("id", "password");
		type(passwordele, "crmsfa");
		WebElement subele = locateElement("class","decorativeSubmit");
		click(subele);
		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement leadele = locateElement("LinkText", "Leads");
		click(leadele);
		WebElement findleads = locateElement("LinkText", "Find Leads");
		click(findleads);
		WebElement firstname = locateElement("xpath", "(//div[@class='x-form-clear-left'])[19]/..//input");
		type(firstname, "Karthi");
		WebElement fndleadbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndleadbutton);
		WebElement fistlink = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
		click(fistlink);
		verifyTitle("View Lead | opentaps CRM");
		WebElement Edit = locateElement("xpath", "//a[contains(text(),'Edit')]");
		click(Edit);
		WebElement editcompanyname = locateElement("id", "updateLeadForm_companyName");
		editcompanyname.clear();
		type(editcompanyname, "Hexaware");
		WebElement update = locateElement("name", "submitButton");
		click(update);
		WebElement Updatedcompany = locateElement("id", "viewLead_companyName_sp");
		getText(Updatedcompany);
		verifyExactText("Hexaware", "Hexaware");
		closeBrowser();
	}
}
