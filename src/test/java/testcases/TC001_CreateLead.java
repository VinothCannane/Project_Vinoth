package testcases;
	import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;

import wdMethods.SeMethods;



	public class TC001_CreateLead extends SeMethods {
		@Test
		public void login(){
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("id","password");
			type(elePassword, "crmsfa");
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);
			WebElement crmlink = locateElement("LinkText", "CRM/SFA");
			click(crmlink);
			WebElement elecrtlead = locateElement("PartialLinkText","Create Lead");
			click(elecrtlead);
			WebElement cmpyname = locateElement("id","createLeadForm_companyName");
			type(cmpyname, "Hexaware Technologies");
			WebElement frstname = locateElement("id","createLeadForm_firstName");
			type(frstname, "Vinoth");
			WebElement lastname = locateElement("id","createLeadForm_lastName");
			type(lastname, "Selvam");
			WebElement src = locateElement("id", "createLeadForm_dataSourceId");
			selectDropDownUsingText(src, "Employee");
			WebElement MKC = locateElement("id", "createLeadForm_marketingCampaignId");
			selectDropDownUsingText(MKC, "Car and Driver");
			WebElement phneno = locateElement("id","createLeadForm_primaryPhoneNumber");
			type(phneno, "9944929907");
			WebElement emailid = locateElement("id","createLeadForm_primaryEmail");
			type(emailid, "vinoth.fuso@gmail.com");
			WebElement submt = locateElement("name","submitButton");
			clickWithNoSnap(submt);
			WebElement fname = locateElement("id", "viewLead_firstName_sp");
			String text=getText(fname);
			verifyExactText(text, "Vinoth");
			}

	}
